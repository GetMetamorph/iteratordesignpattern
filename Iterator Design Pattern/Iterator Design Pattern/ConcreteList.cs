namespace Iterator_Design_Pattern;

public class ConcreteList : ICollection
{
    private List<object> _items;

    public ConcreteList(List<object> items)
    {
        _items = items;
    }

    public IIterator GetIterator()
    {
        return new ListIterator(_items);
    }
}