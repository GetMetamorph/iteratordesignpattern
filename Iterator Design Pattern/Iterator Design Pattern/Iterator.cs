    using System.Collections;

    namespace Iterator_Design_Pattern;

    public class Iterator : IEnumerator
    {
        private readonly object[] _objects;
        private int _position = -1;

        public Iterator(object[] list)
        {
            _objects = list;
        }

        public bool MoveNext()
        {
            _position++;
            return (_position < _objects.Length);
        }

        public void Reset()
        {
            _position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        internal object Current
        {
            get
            {
                if (_position < 0 || _position >= _objects.Length)
                    throw new InvalidOperationException();
                return _objects[_position];
            }
        }
    }
