namespace Iterator_Design_Pattern;

public class Client
{
    public void TraverseCollection(ICollection collection)
    {
        var iterator = collection.GetIterator();
        while (iterator.MoveNext())
        {
            Console.WriteLine(iterator.Current);
        }
    }
}