namespace Iterator_Design_Pattern;

public interface IIterator
{
    bool MoveNext();
    object Current { get; }
}