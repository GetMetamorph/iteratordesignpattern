namespace Iterator_Design_Pattern;

public interface ICollection
{
    IIterator GetIterator();
}