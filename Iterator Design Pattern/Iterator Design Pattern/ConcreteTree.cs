namespace Iterator_Design_Pattern;

public class ConcreteTree : ICollection
{
    private List<object> _nodes;

    public ConcreteTree(List<object> nodes)
    {
        _nodes = nodes;
    }

    public IIterator GetIterator()
    {
        List<object> items = new List<object>();
        foreach (Genre genre in _nodes)
        {
            var genreIterator = genre.GetIterator();
            while (genreIterator.MoveNext())
            {
                items.Add(genreIterator.Current);
            }
        }
        return new TreeIterator(items);
    }
}
