namespace Iterator_Design_Pattern;

public class ListIterator : IIterator
{
    private List<object> _items;
    private int _index = -1;

    public ListIterator(List<object> items)
    {
        _items = items;
    }

    public bool MoveNext()
    {
        _index++;
        return _index < _items.Count;
    }

    public object Current
    {
        get { return _items[_index]; }
    }
}