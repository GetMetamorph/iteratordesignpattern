namespace Iterator_Design_Pattern;

public class Genre
{
    public string Name { get; set; }
    public List<Book> Books { get; set; }
    
    public IIterator GetIterator()
    {
        return new ListIterator(Books.Cast<object>().ToList());
    }
}