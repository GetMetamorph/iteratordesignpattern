﻿using Iterator_Design_Pattern;
using static Iterator_Design_Pattern.Iterator;
// Example d'utilisation 

//On définit une liste de string
Console.WriteLine("Cas 1 : Iterator utilisant IEnumerator<T>");

List<string> list = new List<string>() { "A", "B", "C" };
using IEnumerator<string> iterator = list.GetEnumerator();
while (iterator.MoveNext())
{
    Console.WriteLine(iterator.Current);
}


//On peut aussi utiliser un foreach qui utilise un struct Enumerator
Console.WriteLine("Cas 2 : Iterator utilisant foreach");

foreach (string item in list)
{
    Console.WriteLine(item);
}

//On peut aussi utiliser un itérateur personnalisé
Console.WriteLine("Cas 3 : Iterator personnalisé");
Console.WriteLine("Avec une liste de string");
var myIterator = new Iterator(new object[] { "A", "B", "C" });

while (myIterator.MoveNext())
{
    Console.WriteLine(myIterator.Current);
}

//On peut utiliser plusieurs type de données avec le même itérateur
// Avec un tableau d'entier
Console.WriteLine("Avec un tableau d'entier");
var array = new object[] { 1, 2, 3, 4, 5 };
var arrayIterator = new Iterator(array);
TraverseWithIterator(arrayIterator);

// Avec un tableau de dates
Console.WriteLine("Avec un tableau de dates");
var otherStructure = new object[] { DateTime.Now, DateTime.Now.AddDays(1), DateTime.Now.AddDays(2) };
var otherIterator = new Iterator(otherStructure);
TraverseWithIterator(otherIterator);


static void TraverseWithIterator(Iterator iterator)
{
    while (iterator.MoveNext())
    {
        Console.WriteLine(iterator.Current);
    }

    iterator.Reset();
    Console.WriteLine("---");
}




// Une ConcreteList avec des livres.
var libraryBooks = new ConcreteList(new List<object>
{
    new Book { Title = "Le Petit Prince", Author = "Antoine de Saint-Exupéry" },
    new Book { Title = "1984", Author = "George Orwell" },
    new Book { Title = "To Kill a Mockingbird", Author = "Harper Lee" },
});

// Un ConcreteTree avec des livres organisés par genre.
var booksByGenre = new ConcreteTree(new List<object>
{
    new Genre 
    { 
        Name = "Fiction", 
        Books = new List<Book> 
        { 
            new Book { Title = "1984", Author = "George Orwell" }, 
            new Book { Title = "To Kill a Mockingbird", Author = "Harper Lee" },
        } 
    },
    new Genre 
    { 
        Name = "Fantasy", 
        Books = new List<Book> 
        { 
            new Book { Title = "Le Hobbit", Author = "J.R.R. Tolkien" }, 
            new Book { Title = "Harry Potter et la Pierre Philosophale", Author = "J.K. Rowling" },
        } 
    },
});

void PrintAllBookTitles(ICollection collection)
{
    var iterator = collection.GetIterator();
    while (iterator.MoveNext())
    {
        Book book = (Book)iterator.Current;
        Console.WriteLine(book.Title);
    }
}
Console.Write("LibraryBooks :");
PrintAllBookTitles(libraryBooks);
Console.WriteLine("Books by Genre : ");
PrintAllBookTitles(booksByGenre);
