namespace Iterator_Design_Pattern;

public class Book
{
    public string Title { get; set; }
    public string Author { get; set; }
}