namespace Iterator_Design_Pattern;

public class TreeIterator : IIterator
{
    private List<object> _nodes;
    private int _index = -1;

    public TreeIterator(List<object> nodes)
    {
        _nodes = nodes;
    }

    public bool MoveNext()
    {
        _index++;
        return _index < _nodes.Count;
    }

    public object Current
    {
        get { return _nodes[_index]; }
    }
}